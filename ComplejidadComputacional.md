# Complejidad computacional

**En los siguientes algoritmos planteados en el taller podemos encontrar varias diferencias en su nivel de abstraccion y por consiguiente su complejidad computacional va ser mayor o menor**

> Algoritmo 1 Python

```
from memory_profiler import profile

@profile
def imprimir(lista):
    n = lista
    print(lista)


if "__main__" == __name__:
  lista = 2
  imprimir(lista)
```

> Algoritmo 1 Java

```

 public static void imprimir(int lista) {
 int n = lista;
        System.out.println(lista);
    }
    public static void main(String[] args) {
         int lista = 2;
        imprimir(lista);
    }

```

-   Profiling Python

1s
!mprof run --multiprocess algoritmo1.py
mprof: Sampling memory every 0.1s
running new process
2
Filename: /content/algoritmo1.py

# Line # Mem usage Increment Occurrences Line Contents

     3     36.8 MiB     36.8 MiB           1   @profile
     4                                         def imprimir(lista):
     5     36.8 MiB      0.0 MiB           1       n = lista
     6     36.8 MiB      0.0 MiB           1       print(lista)

![algoritmo1 Python](/graphic-algoritmos/algoritmo1graphic.png)

-   Profiling Java
    ![algoritmo1Java](/graphic-algoritmos/agoritmo1java.PNG)

    ![1JAva](/graphic-algoritmos/algoritmo1javatime.PNG)

Complejidad Temporal Constante
Complejidad Espacial Lineal

> Algoritmo 2 Python

```
from memory_profiler import profile

@profile
def mi_algoritmo(n):
    lista = list(range(n))
    pares = []
    for i in lista:
        for j in lista:
            pares.append((i, j))
    return pares

if __name__ == "__main__":
    mi_algoritmo(1000)
```

> Algoritmo 2 Java

```

package profyler;
import java.util.ArrayList;
import java.util.List;

public class Profyler {

    public static void main(String[] args) {

        List<Pair<Integer, Integer>> pairs = miAlgoritmo(1000);
        // Print the pairs (for demonstration purposes)
        for (Pair<Integer, Integer> pair : pairs) {
            System.out.println(pair);
        }
    }

    public static List<Pair<Integer, Integer>> miAlgoritmo(int n) {
        List<Pair<Integer, Integer>> pairs = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                pairs.add(new Pair<>(i, j));
            }
        }
        return pairs;
    }

    // Define a simple Pair class (you can use Java's built-in Pair class if available)
    static class Pair<T, U> {
        T first;
        U second;

        public Pair(T first, U second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public String toString() {
            return "(" + first + ", " + second + ")";
        }
    }
    }


```

-   Profiling Python

mprof: Sampling memory every 0.1s
running new process
Filename: /content/algoritmo2.py

# Line # Mem usage Increment Occurrences Line Contents

     3     36.8 MiB     36.8 MiB           1   @profile
     4                                         def mi_algoritmo(n):
     5     36.8 MiB      0.0 MiB           1       lista = list(range(n))
     6     36.8 MiB      0.0 MiB           1       pares = []
     7    105.7 MiB      0.0 MiB        1001       for i in lista:
     8    105.7 MiB     58.2 MiB     1001000           for j in lista:
     9    105.7 MiB     10.7 MiB     1000000               pares.append((i, j))
    10    105.7 MiB      0.0 MiB           1       return pares

![algoritmo2py](/graphic-algoritmos/algoritmo2graphic.png)

-   Profiling Java
    ![algoritmo2Java](/graphic-algoritmos/algoritmo2java.png)

    ![2Javatime](/graphic-algoritmos/algoritmo2javatime.png)
    Complejidad Temporal Logaritmica
    Complejidad Espacial Lineal

> Algoritmo 3 Python

```
import random
import time
from memory_profiler import profile

@profile
def operacion_intensiva_memoria(n):
    """Operación que genera un gran uso de memoria temporalmente."""
    gran_lista = [random.random() for _ in range(n)]
    time.sleep(1)  # Simulamos un procesamiento
    return sum(gran_lista)

@profile
def operacion_intensiva_cpu(n):
    """Operación que consume tiempo de CPU."""
    contador = 0
    for _ in range(n):
        contador += random.random()
        time.sleep(0.01)  # Añade un pequeño retardo para simular procesamiento

def main():
    n = 500000
    for i in range(5):
        print(f"Pico {i+1}: Operación intensiva en memoria")
        operacion_intensiva_memoria(n)
        print(f"Pico {i+1}: Operación intensiva en CPU")
        operacion_intensiva_cpu(100)

if __name__ == "__main__":
    main()
```

> Algoritmo 3 Java

```
package profyler;

import java.util.ArrayList;
import java.util.List;

public class Profyler {
    public static void operacionIntensivaMemoria(int n) {
        List<Double> granLista = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            granLista.add(Math.random());
        }
        try {
            Thread.sleep(1000); // Simulate processing
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        double sum = 0;
        for (double num : granLista) {
            sum += num;
        }
    }

    public static void operacionIntensivaCPU(int n) {
        double contador = 0;
        for (int i = 0; i < n; i++) {
            contador += Math.random();
            try {
                Thread.sleep(10); // Add a small delay to simulate processing
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        int n = 500000;
        for (int i = 0; i < 5; i++) {
            System.out.println("Pico " + (i + 1) + ": Operación intensiva en memoria");
            operacionIntensivaMemoria(n);
            System.out.println("Pico " + (i + 1) + ": Operación intensiva en CPU");
            operacionIntensivaCPU(100);

        }
}
}
```

-   Profiling Python

mprof: Sampling memory every 0.1s
running new process
Pico 1: Operación intensiva en memoria
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

     5     37.1 MiB     37.1 MiB           1   @profile
     6                                         def operacion_intensiva_memoria(n):
     7                                             """Operación que genera un gran uso de memoria temporalmente."""
     8     56.4 MiB     19.3 MiB      500003       gran_lista = [random.random() for _ in range(n)]
     9     56.4 MiB      0.0 MiB           1       time.sleep(1)  # Simulamos un procesamiento
    10     56.4 MiB      0.0 MiB           1       return sum(gran_lista)

Pico 1: Operación intensiva en CPU
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

    12     39.8 MiB     39.8 MiB           1   @profile
    13                                         def operacion_intensiva_cpu(n):
    14                                             """Operación que consume tiempo de CPU."""
    15     39.8 MiB      0.0 MiB           1       contador = 0
    16     39.8 MiB      0.0 MiB         101       for _ in range(n):
    17     39.8 MiB      0.0 MiB         100           contador += random.random()
    18     39.8 MiB      0.0 MiB         100           time.sleep(0.01)  # Añade un pequeño retardo para simular procesamiento

Pico 2: Operación intensiva en memoria
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

     5     39.8 MiB     39.8 MiB           1   @profile
     6                                         def operacion_intensiva_memoria(n):
     7                                             """Operación que genera un gran uso de memoria temporalmente."""
     8     56.3 MiB     16.5 MiB      500003       gran_lista = [random.random() for _ in range(n)]
     9     56.3 MiB      0.0 MiB           1       time.sleep(1)  # Simulamos un procesamiento
    10     56.3 MiB      0.0 MiB           1       return sum(gran_lista)

Pico 2: Operación intensiva en CPU
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

    12     42.5 MiB     42.5 MiB           1   @profile
    13                                         def operacion_intensiva_cpu(n):
    14                                             """Operación que consume tiempo de CPU."""
    15     42.5 MiB      0.0 MiB           1       contador = 0
    16     42.5 MiB      0.0 MiB         101       for _ in range(n):
    17     42.5 MiB      0.0 MiB         100           contador += random.random()
    18     42.5 MiB      0.0 MiB         100           time.sleep(0.01)  # Añade un pequeño retardo para simular procesamiento

Pico 3: Operación intensiva en memoria
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

     5     42.5 MiB     42.5 MiB           1   @profile
     6                                         def operacion_intensiva_memoria(n):
     7                                             """Operación que genera un gran uso de memoria temporalmente."""
     8     56.2 MiB     13.7 MiB      500003       gran_lista = [random.random() for _ in range(n)]
     9     56.2 MiB      0.0 MiB           1       time.sleep(1)  # Simulamos un procesamiento
    10     56.2 MiB      0.0 MiB           1       return sum(gran_lista)

Pico 3: Operación intensiva en CPU
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

    12     42.6 MiB     42.6 MiB           1   @profile
    13                                         def operacion_intensiva_cpu(n):
    14                                             """Operación que consume tiempo de CPU."""
    15     42.6 MiB      0.0 MiB           1       contador = 0
    16     42.6 MiB      0.0 MiB         101       for _ in range(n):
    17     42.6 MiB      0.0 MiB         100           contador += random.random()
    18     42.6 MiB      0.0 MiB         100           time.sleep(0.01)  # Añade un pequeño retardo para simular procesamiento

Pico 4: Operación intensiva en memoria
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

     5     42.6 MiB     42.6 MiB           1   @profile
     6                                         def operacion_intensiva_memoria(n):
     7                                             """Operación que genera un gran uso de memoria temporalmente."""
     8     56.3 MiB     13.7 MiB      500003       gran_lista = [random.random() for _ in range(n)]
     9     56.3 MiB      0.0 MiB           1       time.sleep(1)  # Simulamos un procesamiento
    10     56.3 MiB      0.0 MiB           1       return sum(gran_lista)

Pico 4: Operación intensiva en CPU
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

    12     43.6 MiB     43.6 MiB           1   @profile
    13                                         def operacion_intensiva_cpu(n):
    14                                             """Operación que consume tiempo de CPU."""
    15     43.6 MiB      0.0 MiB           1       contador = 0
    16     43.6 MiB      0.0 MiB         101       for _ in range(n):
    17     43.6 MiB      0.0 MiB         100           contador += random.random()
    18     43.6 MiB      0.0 MiB         100           time.sleep(0.01)  # Añade un pequeño retardo para simular procesamiento

Pico 5: Operación intensiva en memoria
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

     5     43.6 MiB     43.6 MiB           1   @profile
     6                                         def operacion_intensiva_memoria(n):
     7                                             """Operación que genera un gran uso de memoria temporalmente."""
     8     56.2 MiB     12.6 MiB      500003       gran_lista = [random.random() for _ in range(n)]
     9     56.2 MiB      0.0 MiB           1       time.sleep(1)  # Simulamos un procesamiento
    10     56.2 MiB      0.0 MiB           1       return sum(gran_lista)

Pico 5: Operación intensiva en CPU
Filename: /content/algoritmo3.py

# Line # Mem usage Increment Occurrences Line Contents

    12     43.6 MiB     43.6 MiB           1   @profile
    13                                         def operacion_intensiva_cpu(n):
    14                                             """Operación que consume tiempo de CPU."""
    15     43.6 MiB      0.0 MiB           1       contador = 0
    16     43.6 MiB      0.0 MiB         101       for _ in range(n):
    17     43.6 MiB      0.0 MiB         100           contador += random.random()
    18     43.6 MiB      0.0 MiB         100           time.sleep(0.01)  # Añade un pequeño retardo para simular procesamiento

![algoritmo3py](/graphic-algoritmos/algoritmo3graphic.png)

-   Profiling Java
    ![algoritmo3Java](/graphic-algoritmos/algoritmo3java.png)

    ![algoritmo3Javatime](/graphic-algoritmos/algoritmo3javatime.png)

Complejidad Espacial Exponencial

> Algoritmo 4 Python

```
def busqueda(arr, elemento_buscado):
    izquierda, derecha = 0, len(arr) - 1
    while izquierda <= derecha:
        medio = (izquierda + derecha) // 2
        medio_valor = arr[medio]

        if medio_valor == elemento_buscado:
            return medio
        elif elemento_buscado < medio_valor:
            derecha = medio - 1
        else:
            izquierda = medio + 1
    return -1

indice = busqueda([1, 2, 3, 4, 5, 6, 7, 8, 9], 7)
```

> Algoritmo 4 Java

```

public class Profyler {

    public static int runBinarySearchIteratively(int[] sortedArray, int key) {
        int left = 0;
        int right = sortedArray.length - 1;

        while (left <= right) {
            int mid = left + (right - left) / 2;

            if (sortedArray[mid] == key) {
                return mid;
            } else if (sortedArray[mid] < key) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return -1; // Element not found
    }

    public static void main(String[] args) {
        int[] arr = {1, 41, 3, 4, 45,7, 99, 888, 11};
        int searchKey = 99;
        int resultIndex = runBinarySearchIteratively(arr, searchKey);
        if (resultIndex != -1) {
            System.out.println("Element found at index " + resultIndex);
        }else {
            System.out.println("Element not present in the array");
                }
        }
    }

```

-   Profiling
    Filename: algoritmo4.py

# Line # Mem usage Increment Occurrences Line Contents

     4     36.8 MiB     36.8 MiB           1   @profile
     5
     6                                         def busqueda(arr, elemento_buscado):
     7     36.8 MiB      0.0 MiB           1       izquierda, derecha = 0, len(arr) - 1
     8     36.8 MiB      0.0 MiB           2       while izquierda <= derecha:
     9     36.8 MiB      0.0 MiB           2           medio = (izquierda + derecha) // 2
    10     36.8 MiB      0.0 MiB           2           medio_valor = arr[medio]
    11
    12     36.8 MiB      0.0 MiB           2           if medio_valor == elemento_buscado:
    13     36.8 MiB      0.0 MiB           1               return medio
    14     36.8 MiB      0.0 MiB           1           elif elemento_buscado < medio_valor:
    15                                                     derecha = medio - 1
    16                                                 else:
    17     36.8 MiB      0.0 MiB           1               izquierda = medio + 1
    18                                             return -1

![algoritmo4py](/graphic-algoritmos/algoritmo4graphic.png)

-   Profiling Java
    ![algoritmo4Java](/graphic-algoritmos/algoritmo4java.png)

    ![algoritmo4Javatime](/graphic-algoritmos/algoritmo4javatime.PNG)

    Complejidad Temporal Logaritmica

> Algoritmo 5 Python

```
def generar_subconjuntos(conjunto):
    subconjuntos = [[]]  # Inicializa con el conjunto vacío
    for elemento in conjunto:
        nuevos_subconjuntos = []
        for subconjunto in subconjuntos:
            nuevo_subconjunto = subconjunto[:]  # Crea una copia del subconjunto actual
            nuevo_subconjunto.append(elemento)  # Agrega el elemento actual al nuevo subconjunto
            nuevos_subconjuntos.append(nuevo_subconjunto)  # Agrega el nuevo subconjunto a la lista de nuevos subconjuntos
        subconjuntos.extend(nuevos_subconjuntos)  # Agrega todos los nuevos subconjuntos a la lista de subconjuntos
    return subconjuntos

# Ejemplo de uso
conjunto = [1, 2, 3]
subconjuntos = generar_subconjuntos(conjunto)
print("Subconjuntos:", subconjuntos)

```

> Algoritmo 5 Java

```

```

-   Profiling
    mprof: Sampling memory every 0.1s
    running new process
    Filename: /content/algoritmo5.py

# Line # Mem usage Increment Occurrences Line Contents

     4     36.9 MiB     36.9 MiB           1   @profile
     5
     6                                         def generar_subconjuntos(conjunto):
     7     36.9 MiB      0.0 MiB           1       subconjuntos = [[]]  # Inicializa con el conjunto vacío
     8     36.9 MiB      0.0 MiB           4       for elemento in conjunto:
     9     36.9 MiB      0.0 MiB           3           nuevos_subconjuntos = []
    10     36.9 MiB      0.0 MiB          10           for subconjunto in subconjuntos:
    11     36.9 MiB      0.0 MiB           7               nuevo_subconjunto = subconjunto[:]  # Crea una copia del subconjunto actual
    12     36.9 MiB      0.0 MiB           7               nuevo_subconjunto.append(elemento)  # Agrega el elemento actual al nuevo subconjunto
    13     36.9 MiB      0.0 MiB           7               nuevos_subconjuntos.append(nuevo_subconjunto)  # Agrega el nuevo subconjunto a la lista de nuevos subconjuntos
    14     36.9 MiB      0.0 MiB           3           subconjuntos.extend(nuevos_subconjuntos)  # Agrega todos los nuevos subconjuntos a la lista de subconjuntos
    15     36.9 MiB      0.0 MiB           1       return subconjuntos

Subconjuntos: [[], [1], [2], [1, 2], [3], [1, 3], [2, 3], [1, 2, 3]]

![algoritmo5py](/graphic-algoritmos/algoritmo5graphic.png)

-   Profiling Java
    ![algoritmo5Java](/graphic-algoritmos/algoritmo5java.png)

    ![algoritmo5Javatime](/graphic-algoritmos/algoritmo5javatime.PNG)

> Complejidad Temporal Logaritmica

> ### Comparacion en Tiempo
>
> | Num Algoritmo | Tiempo en Python | Tiempo en Java |
> | ------------- | ---------------- | -------------- |
> | 1             | 0.6 seg          | 0.221 seg      |
> | 2             | 120 seg          | 0.017276 seg   |
> | 3             | algo             | alg            |
> | 4             | algo             | alg            |
> | 5             | algo             | alg            |

> ### Comparacion en Memoria
>
> | Num Algoritmo | Memoria en Python | Memoria en Java |
> | ------------- | ----------------- | --------------- |
> | 1             | 36.8 Mib          |                 |
> | 2             | 105.7 Mib         | alg             |
> | 3             | 493.5 Mib         | alg             |
> | 4             | 36.8 Mib          | alg             |
> | 5             | 36.9 Mib          | alg             |
